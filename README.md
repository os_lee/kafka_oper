# kafka_oper

#### 介绍

- 使用go语言操作kafka 
- 使用docker-compose快速搭建kafka3集群环境

#### 使用

- 可使用kraft-docker-compose.yml快速搭建一套kafka3集群环境,无zookeeper
````
- docker network create --driver bridge --subnet 172.23.0.0/16 --gateway 172.23.0.1 kafka-cluster
- docker-compose -f kraft-docker-compose.yml up -d
- 注意修改宿主机IP:10.128.160.60改为自己的IP
````
- 初始化
````
- go mod init lee/kafka_oper
- go mod tidy
````
- 具体操作查看main方法
- 附带web工具kafka3_monitor localhost:8082

#### 作者

lee

#### 捐助

如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。
![](zfb.jpg)

